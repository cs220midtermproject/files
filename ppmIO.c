//ppmIO.c                                                                                                                                                                                                   
//601.220, Spring 2018                                                                                                                                                                          
//Starter code for midterm project - feel free to edit/add to this file                                                                                                   
#include <stdio.h>
#include <stdlib.h>
#include "ppmIO.h"

/* Creates an empty PPM formatted image from file (assume fp != NULL) */
Image* createImage(FILE *fp) {
	Image *im = (Image*)malloc(sizeof(Image));
	fscanf(fp, "P6\n");
	int count = 0;

	// Checks for comments
	while(1) { 
		char comment[100]; //Assume max comment line is 100 characters 
		int num;
		int nread = fscanf(fp, "%d\n", &num);
		if(nread == 0) {
			fgets(comment, 100, fp);
		}
		else {
			if(count == 0) {
				im->cols = num;
			}
			else if(count == 1) {
				im->rows = num;
			}
			count = count +  1;
		}
		if(count > 2) {
			break;
		}
	}
	im->data = malloc(sizeof(Pixel) * (im->cols) * (im->rows));
	return im;
}

/* Reads file into Image img */
int readPPM(FILE *fp, Image *im) {
	int num_pixels_read = (int)fread(im->data, sizeof(Pixel), (im->rows) * (im->cols), fp);

	// Checking for error with reading the file
	if(num_pixels_read != (im->rows) * (im->cols)) {
		printf("Failure to read input \n");
		return -1;
	}
	else if (num_pixels_read == 0) {
		printf("Failure to read input\n");
		return -1;
	}
	fclose(fp);
	return 0;
}

/* Write PPM formatted image to a file (assumes fp != NULL and img != NULL) */
int writePPM(FILE *fp, const Image* im) {

	/* Abort if either file pointer is dead or image pointer is null; indicate failure with -1 */
	if (!fp || !im) {
		return -1;
	}
	
	/* Write tag and dimensions; colors is always 255 */
	fprintf(fp, "P6\n%d %d\n%d\n", im->cols, im->rows, 255);

	/* Write pixels */
	int num_pixels_written = (int) fwrite(im->data, sizeof(Pixel), (im->rows) * (im->cols), fp);

	/* Check if write was successful or not; indicate failure with -1 */
	if (num_pixels_written != (im->rows) * (im->cols)) {
		return -1;
	}

	/* Success, so return number of pixels written */
	return num_pixels_written;
}
