#include <stdio.h>
#include <stdlib.h>

/* Function to swap color channels for picture */
void swap(Image* im) {
	printf("Swap Function\n");
	unsigned char temp;

	// Finds the location of the pixel in the struct array
	int rows = im->rows;
	int cols = im->cols;

	// Moves the values of rgb to gbr
	for(int i = 0; i < rows * cols; i++) {
		temp = im->data[i].r;
		im->data[i].r = im->data[i].g;
		im->data[i].g = im->data[i].g;
		im->data[i].b = temp;
	}

}

// Function to blackout top right corner of picture
void blackout(Image* im) {
	printf("Blackout Function\n");

	// Finds the location of the pixel in the struct array
	int rows = im->rows;
	int cols = im->cols;

	// Blackout top right corner
	for(int r = 0; r < rows; r++) {
		for(int c = 0; c < cols; c++) {
			if(r < rows / 2 && c > cols / 2) {
				im->data[r*cols + c].r = 0;
				im->data[r*cols + c].g = 0;
				im->data[r*cols + c].b = 0;
			}
		}
	}		
}	

// Function to convert picture to greyscale
void grayscale(Image* im) {
	printf("Grayscale Function\n");
	
	// Finds the location of the pixel in the struct array
	int rows = im->rows;
	int cols = im->cols;

	// Grayscale conversion
	for(int i = 0; i < rows * cols; i++) {
		float red = (float)(im->data[i].r);
		float green = (float)(im->data[i].g);
		float blue = (float)(im->data[i].b);
		float intensity = 0.30*red + 0.59*green + 0.11*blue;
		im->data[i].r = intensity;
		im->data[i].g = intensity;
		im->data[i].b = intensity;
	}
}

// Function to crop image
Image* crop(Image* im, int x1, int y1, int x2, int y2) {
	printf("Crop Function\n");
	
	// Finds the location of the pixel in the struct array
	int rows = im->rows;
	int cols = im->cols;
	
	// Calculates row and column values for cropped picture
	int cropRows = y2 - y1 + 1;
	int cropCols = x2 - x1 + 1;

	// Allocating new cropped image memory
	Image* im2 = (Image*)malloc(sizeof(Image));
	im2->rows = cropRows;
        im2->cols = cropCols;
	im2->data = malloc(sizeof(Pixel) * (im2->rows) * (im2->cols));

	// Moving pixels from original picture to 
	for(int c = x1; c <= x2; c++) {
		for(int r = y1; r <= y2; r++) {
			im2->data[(r-y1)*(cropCols)+(c-x1)].r = im->data[r*cols+c].r;
                        im2->data[(r-y1)*(cropCols)+(c-x1)].g = im->data[r*cols+c].g;
                        im2->data[(r-y1)*(cropCols)+(c-x1)].b = im->data[r*cols+c].b;
		}
	}
	
	// Deallocating memory stored in original image
	free(im->data);
	free(im);
	return im2;
}

void helperfunction(double *saturate) { 
	if(*saturate > 255) {
		*saturate = 255;
	}
	if(*saturate < 0){
		*saturate = 0;
	}
}

// Function to adjust contrast of image
void contrast(Image* im, double factor) {
	printf("Contrast Function\n");
	
	double span = 255 / 2;
	int rows = im->rows;
	int cols = im->cols;
	double num, num1, num2;
	double * num3;
	double * num4;
	double * num5;

	for(int i = 0; i < rows * cols; i++) {
		num = (im->data[i].r - 128) * factor + 128;
		num1 = (im->data[i].g - 128) * factor + 128;
		num2 = (im->data[i].b - 128) * factor + 128;
		
		num3 = &num;
		num4 = &num1;
		num5 = &num2;

		helperfunction(num3);
		helperfunction(num4);
		helperfunction(num5);

		im->data[i].r = (int)num;
		im->data[i].g = (int)num1;
		im->data[i].b = (int)num2;
	}
}
